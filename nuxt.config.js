const axios = require('axios')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  globalName: 'elborso',
  ssr: true,
  router: {

    routeNameSplitter: '/',
    //prefetchPayloads: false
  //  mode: 'hash'
  //  middleware: 'routeDelHash'
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Сумки и аксессуары ручной работы из высококачественной натуральной кожи',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        name: 'yandex-verification',
        content: 'b5a829a01295f124'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Интернет-магазин ELBORSO предлагает оригинальные сумки и аксессуары ручной работы из высококачественной натуральной кожи.'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'купить женскую сумку, купить мужскую сумку, кросс-боди, багет, сумка на плечо, из кожи, деловой, стиль, вечерний, весна лето 2021, натуральная кожа, мессенджер, сумочка'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }],
    script: [
      { hid: 'stripe', src: '//cdn.callibri.ru/callibri.js', body: true }
     ],
  },
  publicRuntimeConfig: {
    axios: {
//baseURL: 'http://api.elborso/'
baseURL: process.env.API_URL
    }
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/app.scss'
  //  '@node_modules/vue-slick-carousel/dist/vue-slick-carousel'
  ],

  loadingIndicator: {
    name: 'circle',
    color: 'black',
    background: 'white'
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/b24.js'},
    {
      src: '~/plugins/persistedState.client.js'
    },
    { src: './plugins/vue-slick.js' },
/*    {
      src: '~/plugins/gsap.js'
    },*/
    {
      src: '~/plugins/switch.js'
    },
    {
      src: '~/plugins/ranger.js',
      ssr: false
    },
    {  src: '~/plugins/mask.js'},
    {  src: '~/plugins/scrollvue.js'},
  //  {  src: '~/plugins/charts.js'},
  //  {  src: '~/plugins/barchart.js'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
     '@nuxtjs/google-analytics'
   ],
   googleAnalytics: {
    id: 'UA-196558151-1'
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/google-gtag',

    ['@nuxtjs/recaptcha', {
      hideBadge: true,
      siteKey: '6Le4GbAaAAAAAIIWsxqFosHvOrK3KaEdjEVv3AIS',

      version: 2,
    }],
    ['vue-scrollto/nuxt', { duration: 300 }],

    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '860807558002697',
      autoPageView: true,
      disabled: false,
      debug:true
    }],
    [
     '@nuxtjs/yandex-metrika',
     {
       id: '69095230',
       webvisor: true,
       clickmap:true,
        trackHash:true,
        trackLinks:true,
       accurateTrackBounce:true,
     }
   ],
    // https://go.nuxtjs.dev/bootstrap
    ['bootstrap-vue/nuxt', {
      componentPlugins: [
      'LayoutPlugin',
      'FormRating',
      'FormCheckboxPlugin',
      'FormInputPlugin',
      'FormRadioPlugin',
      'ModalPlugin',
      'CarouselPlugin',
      'DropdownPlugin',
      'BreadcrumbPlugin',
      'NavPlugin',
      'NavbarPlugin',
      'CollapsePlugin',
      'InputGroupPlugin',
      'ButtonPlugin',
      'TooltipPlugin',
      'CardPlugin',
      'FormDatepickerPlugin'

    ],
      css: false,
      icons: false
    }],
    ['@nuxtjs/axios'],
    ['nuxt-i18n'],
     '@nuxtjs/sitemap'
  ],
  'google-gtag': {
      id: 'AW-379798474',
      debug: true,
    },
  sitemap: {
    path: '/sitemap.xml',
     hostname: 'https://elborso.ru',

     i18n: true,
     i18n: {
       locales: ['en'],
       routesNameSeparator: '___'
     },

    sitemaps: [
      {
        exclude: [
        '/analytics'
      ],
        path: '/sitemapcatalog.xml',
        routes: async () => {
           const { data } = await axios.get('https://api.elborso.ru/catalog')
           return data.map((product) => `/catalog/${product.category}/${product.title}/${product.id}`)
         }
      },
      {
        exclude: [
        '/analytics'
      ],
        path: '/sitemapblog.xml',
        routes: async () => {
           const { data } = await axios.get('https://api.elborso.ru/blog')
           return data.map((product) => `/blog/${product.id}`)
         }
      }]
     // options
   },/* */

  i18n: {
    locales: [{
        code: 'en',
        file: 'en-US.js'
      },
      {
        code: 'ru',
        file: 'ru-RU.js'
      },
    ],
    lazy: true,
    useCookie: true,
    langDir: 'locality/',
    defaultLocale: process.env.LANG_SERV,
  },
  dev: {
   useEslint: false,

 },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    //analyze: true,
    extractCSS: true,
    splitChunks: {
     layouts: false,
     pages: true,
     commons: true
   },
    publicPath: '/nuxt/',
    transpile: ['gsap'],

    babel: {
      compact: true
    }
  }
}
